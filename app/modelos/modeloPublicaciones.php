<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
class modeloPublicaciones{
    private $db;
    public function __construct() {
        $this->db = new Conexion();
    }
    
    public function selPublicaciones() {
     $query = "SELECT p.*, i.nombre_img, i.foto_img, i.id_imagen FROM producto p INNER JOIN imagen i on i.idProducto=p.id_producto";
     return $this->db->resultquery($query);
    }
    
    public function publicacionesPorId($id) {
        $query = $this->db->query("SELECT p.*, i.nombre_img, i.foto_img, i.id_imagen FROM producto p INNER JOIN imagen i on i.idProducto=p.id_producto WHERE p.id_producto =$id");
    $row = $query->fetch_object();
    return $row;        
    }
    
    public function agregarPublicaciones($data) {
        $nombre = $data['nombre'];
        $stock = $data['stock'];
        $color = $data['color'];
        $talla = $data['talla'];
        $precio = $data['precio'];
        $imagen = $data['imagen'];
        $cn = mysqli_connect('127.0.0.1:33062', 'root', "", 'proyecto') or die ("No se ha podido conectar al servidor de Base de datos");
        mysqli_set_charset($cn, 'utf8');//conexion directa para recuperar los datos
        
        mysqli_query($cn,"INSERT INTO `producto`(`id_producto`, `nombre_prod`, `color`, `talla`, `precio`, `stock`) VALUES "
                . "(null,'$nombre','$color','$talla',$precio,$stock)");
        
        $id = mysqli_insert_id($cn);
        
        $ins1 = $this->db->query("INSERT INTO `imagen`(`id_imagen`, `nombre_img`, `foto_img`, `idProducto`) VALUES "
                . "(null,'$imagen','$imagen',$id)");
      if ($ins1){
          return true;
      }  else{
          return false;
      }
    }
    
    public function agregarOrden($data) {
        $id_perfil = $data['id_perfil'];
        $id_producto = $data['id_producto'];
        $cantidad = $data['cantidad'];
        $ids = $_SESSION['usuario'];

        $cn = mysqli_connect('127.0.0.1:33062', 'root', "", 'proyecto') or die ("No se ha podido conectar al servidor de Base de datos");
        mysqli_set_charset($cn, 'utf8');
        
        $todo = mysqli_query($cn,"SELECT * FROM `orden` WHERE `codIngreso`='$ids' and estado = 0");
        $todo1 = mysqli_query($cn,"SELECT * FROM producto WHERE id_producto=$id_producto");
        $todo2 = mysqli_fetch_assoc($todo1);
        $todo3 = $todo2["stock"]-$cantidad;
        if(mysqli_num_rows($todo)<=0){
            if($todo3>=0){
                mysqli_query($cn,"INSERT INTO `orden`(`id_orden`, `fecha_orden`, `codIngreso`,estado) VALUES (null, now(), '$ids',0)");
                $id = mysqli_insert_id($cn);
                $ins1 = $this->db->query("INSERT INTO `detalle_orden`(`id_detalle`, `cantidad`, `idOrden`, `idProducto`) VALUES "
                        . "(null, $cantidad, $id, $id_producto)");
                $ins2 = $this->db->query("UPDATE producto SET stock=stock-$cantidad WHERE id_producto=$id_producto");
            }else{
                $ins1="";
            }
        }else{
            if($todo3>=0){
                $todom = mysqli_fetch_assoc($todo);
                $id2 = $todom["id_orden"];
                $ins1 = $this->db->query("INSERT INTO `detalle_orden`(`id_detalle`, `cantidad`, `idOrden`, `idProducto`) VALUES "
                        . "(null, $cantidad, $id2, $id_producto)");
                $ins2 = $this->db->query("UPDATE producto SET stock=stock-$cantidad WHERE id_producto=$id_producto");
            }else{
                $ins1="";
            }
        }
      if ($ins1){
          return true;
      }  else{
          return false;
      }
    }
    
    public function actualizarPublicacion($data) {
    $id_producto = $data['id_producto'];
    $nombre = $data['nombre_prod'];
    $stock = $data['stock'];
    $color = $data['color'];
    $talla = $data['talla'];
    $precio = $data['precio'];
    $imagen = $data['nombre_img'];
    
    $up = $this->db->query("UPDATE `producto` SET `nombre_prod`='$nombre',`color`='$color',`talla`='$talla',`precio`=$precio,`stock`=$stock WHERE id_producto=$id_producto");
    $up1 = $this->db->query("UPDATE `imagen` SET `nombre_img`='$imagen',`foto_img`='$imagen' WHERE idProducto=$id_producto");
    if($up1){
        return true;
    }else{
        return false;
    }
    
        
    }
    public function borrarPublicacion($id, $imagen) {
        $this->db->query("DELETE FROM `detalle_orden` WHERE `idProducto`=$id");
        $this->db->query("DELETE FROM `imagen` WHERE `id_imagen` = '$imagen'");
        $del1 = $this->db->query("DELETE FROM `producto` WHERE `id_producto` = $id");
        if ($del1){
            return true;
        }else{
            return false;
        }
        
    }
}
?>
