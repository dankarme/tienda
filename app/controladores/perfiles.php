<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
class Perfiles extends Controlador {
    public function __construct() {
        if(!isset($_SESSION['id_usuario'])){
            redirect('usuarios/login');
        }
        $this->modeloPerfiles = $this->modelo('modeloPerfiles'); 
    }
    public function index() {
        $posts = $this->modeloPerfiles->selPublicaciones();
        $data = ['post' => $posts];
        $this->vista('perfiles/index', $data);
        
    }
    public function vistaAdministrador($id) {
        $this->modeloPerfiles->admin($id);
        redirect('alertas/seradmin');
    }
    public function vistaUsuario($id) { 
        $this->modeloPerfiles->usuario($id);
        redirect('alertas/serausu');
    }
    public function vistaeditarPerfil($id) {
        $post = $this->modeloPerfiles->perfilesPorId($id);
        $data = [
            'cedula' => $id,
            'nombres' => $post->nombres,
            'apellidos' => $post->apellidos,
            'direccion' => $post->direccion,
            'correo' => $post->correo,
            'telefono1' => $post->telefono1,
            'telefono2' => $post->telefono2,
        ];
        $this->vista('perfiles/editar_perfil', $data);
    }
    
    public function editarPerfil($id) {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = [
            'cedula' => $id,
            'nombres' => trim($_POST['nombres']),
            'apellidos' => trim($_POST['apellidos']),
            'direccion' => trim($_POST['direccion']),
            'correo' => trim($_POST['correo']),
            'telefono1' => trim($_POST['telefono1']),
            'telefono2' => trim($_POST['telefono2']),
        ];
        if ($this->modeloPerfiles->actualizarPerfil($data)) {
            redirect('alertas/edito_perfil');
        } else {
            die('Ocurrio un error');
        }
    }
}
?>
