<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

class Publicaciones extends Controlador {

    public function __construct() {
        if (!isset($_SESSION['id_usuario'])) {
            redirect('usuarios/login');
        }
        $this->modeloPublicacion = $this->modelo('modeloPublicaciones');
    }

    public function index() {
        $posts = $this->modeloPublicacion->selPublicaciones();
        $data = ['post' => $posts];
        $this->vista('publicaciones/index', $data);
    }

    public function vistaNuevaPublicacion() {

        $this->vista('publicaciones/nueva_publicacion');
    }

    public function nuevaPublicacion() {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $archivo = $_FILES['imagen']['tmp_name'];
        $nombrearchivo = $_FILES['imagen']['name'];
        $info = pathinfo($nombrearchivo);
        $extension = $info['extension'];
        $nombre_imagen = $_SESSION['id_usuario'] . '-' . rand(00000, 99999);
        move_uploaded_file($archivo, RUTAIMG . '/public/imagenes/' . $nombre_imagen . '.' . $extension);
        $ruta = $nombre_imagen . '.' . $extension;

        $data = [
            'nombre' => trim($_POST['nombre']),
            'color' => trim($_POST['color']),
            'talla' => trim($_POST['talla']),
            'precio' => trim($_POST['precio']),
            'stock' => trim($_POST['stock']),
            'imagen' => trim($ruta)
        ];


        if ($this->modeloPublicacion->agregarPublicaciones($data)) {

            redirect('alertas/agrego_publicacion');
        } else {
            die('No guardo la publicacion');
        }
    }

    public function subirOrden() {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = [
            'id_perfil' => trim($_POST['id_perfil']),
            'id_producto' => trim($_POST['id_producto']),
            'cantidad' => trim($_POST['cantidad'])
        ];
        if ($this->modeloPublicacion->agregarOrden($data)) {
            redirect('alertas/agrego_orden');
        } else {
            redirect('alertas/stockmal');
        }
    }

    public function vistaEditarPublicacion($id) {
        $post = $this->modeloPublicacion->publicacionesPorId($id);
        if ($_SESSION['id_perfil'] == 2) {
            redirect('publicaciones');
        }
        $data = [
            'id_producto' => $id,
            'nombre_prod' => $post->nombre_prod,
            'color' => $post->color,
            'talla' => $post->talla,
            'precio' => $post->precio,
            'stock' => $post->stock,
            'nombre_img' => $post->nombre_img
        ];
        $this->vista('publicaciones/editar_publicacion', $data);
    }

    public function eliminarPublicacion($id, $imagen) {
        if ($this->modeloPublicacion->borrarPublicacion($id, $imagen)) {
            $borrar_img = RUTAIMG . '/public/imagenes/' . $imagen;
            unlink($borrar_img);
            redirect('alertas/elimino_publicacion');
        } else {
            die('No se pudo eliminar la publicacion');
        }
    }

    public function editarPublicacion($id) {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $archivo = $_FILES['imagen']['tmp_name'];

        if ($archivo == '') {
            $ruta = $_POST['ruta'];
        } else {
            $imagen_eliminar = $_POST['ruta'];
            $borrar_img = RUTAIMG . '/public/imagenes/' . $imagen_eliminar;
            unlink($borrar_img);
            $nombrearchivo = $_FILES['imagen']['name'];
            $info = pathinfo($nombrearchivo);
            $extension = $info['extension'];
            $nombre_imagen = $_SESSION['id_usuario'] . '-' . rand(00000, 99999);
            move_uploaded_file($archivo, RUTAIMG . '/public/imagenes/' . $nombre_imagen . '.' . $extension);
            $ruta = $nombre_imagen . '.' . $extension;
        }
        $data = [
            'id_producto' => $id,
            'nombre_prod' => trim($_POST['nombre_prod']),
            'color' => trim($_POST['color']),
            'talla' => trim($_POST['talla']),
            'precio' => trim($_POST['precio']),
            'stock' => trim($_POST['stock']),
            'nombre_img' => $ruta
        ];

        if ($this->modeloPublicacion->actualizarPublicacion($data)) {
            redirect('alertas/edito_publicacion');
        } else {
            die('Ocurrio un error');
        }
    }

}
?>
