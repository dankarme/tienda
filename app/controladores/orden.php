<?php

class Orden extends Controlador {

    public function __construct() {
        if (!isset($_SESSION['id_usuario'])) {
            redirect('usuarios/login');
        }
        $this->modeloOrden = $this->modelo('modeloOrden');
    }

    public function index() {
        $posts = $this->modeloOrden->selOrden();
        $data = ['post' => $posts];
        $this->vista('orden/index', $data);
    }
    
    public function compraOrd() {
        $this->modeloOrden->comprarOrden();
        redirect('alertas/compraorden');
    }
    public function eliminarOrden($id, $idp, $cantidad) {
        if ($this->modeloOrden->borrarOrden($id, $idp, $cantidad)) {
            redirect('alertas/elimino_orden');
        } else {
            die('No se pudo eliminar la publicacion');
        }
    }
}
?>