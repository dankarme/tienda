<?php
class Usuarios extends Controlador {
    public function __construct() {
        $this->modeloUsuario = $this->modelo('modeloUsuarios');   
    }
    
    public function index() {
        if ($this->checarLogeo()){
            redirect('publicaciones');
        }else{
            $this->vista('usuarios/login');  
        }
        
    }
    public function checarLogeo(){
      if (isset($_SESSION['id_usuario'])){
          return true;
        }else{
           return false;  
        }  
    }
        public function registro() {
        
            $this->vista('usuarios/registro');  
        }
        
        public function registrarse() { 
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = [
            'cedula' => trim($_POST['cedula']),
            'nombre' => trim($_POST['nombre']),
            'apellido' => trim($_POST['apellido']),
            'correo' => trim($_POST['correo']),
            'direccion' => trim($_POST['direccion']),
            'telefono1' => trim($_POST['telefono1']),
            'telefono2' => trim($_POST['telefono2']),
            'usuario' => trim($_POST['usuario']),
            'pass' => trim($_POST['pass']),
        ];
        
        $data['pass'] = password_hash($data['pass'], PASSWORD_DEFAULT);
         
         if ($this->modeloUsuario->altaDeUsuario($data)) {
             $this->vista('usuarios/login');
         }else{
             redirect('alertas/repetido');
         }
        }
        public function iniciarSesion() {
            $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING); 
            $data = [
             'usuario' => trim($_POST['usuario']),
            'pass' => trim($_POST['pass']) 
            ];
            $usuario_logeado = $this->modeloUsuario->login($data['usuario'],$data['pass']); 
            if ($usuario_logeado) {
             $this->crearSesionDeUsuario($usuario_logeado);
         }else{
            redirect('alertas/error');
         }
        }
        
        public function crearSesionDeUsuario($user) {
        $_SESSION['id_usuario'] = $user->cedRegistro;
        $_SESSION['id_perfil'] = $user->id_perfil;
        $_SESSION['usuario'] = $user->usuario;
        redirect('alertas/entro');
             
        }
        public function salir() {
        unset($_SESSION['id_usuario']);
        
        session_destroy();
        redirect('usuarios/login');
             
        }
 
    
}
?>
