<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="../../public/imagenes/adidas.jpg" width="100px;" height="50px;"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php if (isset($_SESSION['id_usuario'])): ?>
            <?php if ($_SESSION['id_perfil'] == 1) { ?>
                <li  class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/perfiles/index">USUARIOS</a></li> 
            <?php } ?>    
            <?php if ($_SESSION['id_perfil'] != 1) { ?>
                <li  class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/orden/index">MIS ORDENES DE COMPRA</a></li> 
            <?php } ?>   
        <?php endif ?>
    </ul>
    <ul class="navbar-nav form-inline my-2 my-lg-0">
      <?php if (isset($_SESSION['id_usuario'])): ?> 
            <li  class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/publicaciones/index">INICIO</a></li>
            <li class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/usuarios/salir">SALIR</a></li>
        <?php else: ?>
            <li class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/usuarios/login">ENTRAR</a></li>
            <li class="nav-item active"><a class="nav-link" href="<?php echo RUTAPUBLIC; ?>/usuarios/registro">REGISTRARSE</a></li>
        <?php endif ?>
    </ul>
  </div>
</nav>