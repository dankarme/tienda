<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php'; ?>
<div class="card">
    <div class="card-content black-text" style="padding: 1px;">
        <span class="card-title black-text"><h2>Lista de mis ordenes de compra</h2></span>
    </div>   
</div>
<div class="container">
    <div class="roww">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Fecha de pedido</th>
                    <th scope="col">Nombre del producto</th>
                    <th scope="col">Color</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Talla</th>
                    <th scope="col">Stock</th>
                    <th scope="col" style="text-align: center;">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $a = 0;
                foreach ($data['post'] as $post): $a++
                    ?>
                    <tr>
                        <td><?= $a ?></td>
                        <td><?php echo $post->cantidad; ?></td>
                        <td><?php echo $post->fecha_orden; ?> </td>
                        <td><?php echo $post->nombre_prod; ?> </td>
                        <td><?php echo $post->color; ?></td>
                        <td><?php echo $post->precio; ?> </td>
                        <td><?php echo $post->talla; ?> </td>
                        <td><?php echo $post->stock; ?> </td>
                        <td>
                        <td title="Hacer usuario" style="text-align: center;font-size: 21px;cursor:pointer;">
                            <a href="<?php echo RUTAPUBLIC; ?>/orden/eliminarOrden/<?php echo $post->id_detalle ?>/<?php echo $post->idProducto ?>/<?php echo $post->cantidad ?>"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>
<?php endforeach ?>
            </tbody>
        </table>
    </div>
    <?php
    if ($data['post']) {
        ?>
        <a href="<?php echo RUTAPUBLIC; ?>/orden/compraOrd">
            <button class="btn btn-primary btnpdf">Realizar compra</button>
        </a>
        <?php
    }
    ?>

</div>
</main>
<script 
    src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQ1LNfOu91ta32o/NMZx1twRo8QtmkMRdAu8="
    crossorigin="anonymous">
</script>
<script src="<?php echo RUTAPIBLIC ?>/public/js/materialize.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
        $(document).on("click", ".btnpdf", function () {
            window.open('http://localhost/impresion/orden.php?usuario=<?= $_SESSION['usuario'] ?>', '_blank');
        });
</script>
</body>
</html>
