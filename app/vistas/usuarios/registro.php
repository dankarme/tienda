
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script>
            function soloLetras(e) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
        <script>
            function soloNumeros(n) {
                key = n.keyCode || n.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " 1234567890,";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
    </head>
    <body>
        <?php
        require RUTAAPP . '/vistas/includes/header.php';
        ?>
        <div class="container" align="center" style="margin-top: 150px;">
            <div class="row center-div">
                <div class="col s12">
                    <div class="card">
                        <div class="row"><br>
                            <h1>Registrarse1</h1>

                        </div>
                        <div class="card-content black-text">
                            <form action="<?php echo RUTAPUBLIC ?>/usuarios/registrarse" method="post" autocomplete="off">
                                <input type="text" name="cedula" placeholder="CEDULA" required="" maxlength="10" autofocus>
                                <input type="text" name="nombre" placeholder="NOMBRE" required onkeypress ="return soloLetras(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="apellido" placeholder="APELLIDO" required onkeypress ="return soloLetras(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="correo" placeholder="CORREO" required onkeypress ="return soloLetras(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="direccion" placeholder="DIRECCION" required onkeypress ="return soloLetras(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="telefono1" placeholder="TELEFONO1" required onkeypress ="return soloNumeros(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="telefono2" placeholder="TELEFONO2"required onkeypress ="return soloNumeros(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="text" name="usuario" placeholder="USUARIO" required onkeypress ="return soloLetras(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <input type="password" name="pass" placeholder="CLAVE" required onkeypress ="return soloNumeros(event)"
                                       oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                                       oninput="setCustomValidity('')"/>
                                <button class="btn waves-effect waves-light black" type="submit" name="action"> REGISTRARSE
                                    <i class="material-icons">send</i></button> 

                            </form>
                        </div>

                    </div> 

                </div>    
            </div>
        </div>
        <?php require RUTAAPP . '/vistas/includes/footer.php'; ?>
    </body>
</html>
