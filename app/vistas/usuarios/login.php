<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link  href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link  href="<?php echo RUTAPUBLIC; ?>/public/css/style.css" rel="stylesheet">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" ></script>
        <title><?php echo NOMBREAPP ?></title>
    </head>
    <body>
        <section id="wrapper" class="login-register login-sidebar" style="background-image:url(https://i.pinimg.com/564x/04/99/04/049904f2779780f2d3a41640868d0bbe.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form action="<?php echo RUTAPUBLIC ?>/usuarios/iniciarSesion" method="post">
                    <a href="javascript:void(0)" class="text-center db"><img src="https://i.pinimg.com/originals/56/1a/30/561a30addb6bd05b7a8ca90d956afbf0.gif" width="350px;" alt="Home"></a> 
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" name="usuario" type="text" required="" placeholder="Usuaurio">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" name="pass" type="password" required="" placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">INGRESAR</button>
                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>No tienen una cuenta? <a href="<?php echo RUTAPUBLIC; ?>/usuarios/registro" class="text-primary m-l-5"><b>Regístrate</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    </body>
</html>

