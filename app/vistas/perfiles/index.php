<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php';?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="./">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lista de usuarios</li>
    <button class="btn btn-primary" style="right: 15px;position: absolute;margin-top: -6px;"><i class="fa fa-plus"></i></button>
  </ol>
</nav>
<div class="container">
    <div class="roww">        
        <table class="table table-bordered table-dark">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Telefonos</th>
                    <th scope="col">Perfil</th>
                    <th scope="col" style="text-align: center;">Opciones</th>
                </tr>
            </thead>
            <tbody>
    <?php $a=0; foreach ($data['post'] as $post): $a++?>
                <tr>
                    <td><?=$a?></td>
                    <td><?php echo $post->nombres; ?> <?php echo $post->apellidos; ?></td>
                    <td><?php echo $post->direccion; ?> </td>
                    <td><?php echo $post->correo; ?> </td>
                    <td><?php echo $post->telefono1; ?>/<?php echo $post->telefono2; ?></td>
                    <td><?php echo $post->perfil; ?> </td>
                    <?php if($_SESSION['id_usuario']==1754411286) { ?>
                        <?php if($post->perfil != "admin") { ?>
                        <td style="text-align: center;font-size: 21px;cursor:pointer;">
                            <a title="Cambiar perfil" href="<?php echo RUTAPUBLIC; ?>/perfiles/vistaAdministrador/
                               <?php /*echo RUTAPUBLIC;*/ echo $post->cedula ?>"><i class="fas fa-address-book"></i></a>
                        <?php }else{ ?>
                         <td style="text-align: center;font-size: 21px;cursor:pointer;">
                            <a title="Cmbiar perfil" href="<?php echo RUTAPUBLIC; ?>/perfiles/vistaUsuario/
                               <?php /*echo RUTAPUBLIC;*/ echo $post->cedula ?>"><i class="fas fa-address-book"></i></a>
                        <?php }}else{?>
                             <td style="text-align: center;font-size: 21px;cursor:pointer;">
                         <?php }?>    
                        <a title="Editar" href="<?php echo RUTAPUBLIC; ?>/perfiles/vistaeditarPerfil/
                           <?php /*echo RUTAPUBLIC;*/ echo $post->cedula ?>"><i class="fas fa-edit"></i></a>
                    </td>
                </tr>
    <?php endforeach ?>
                            </tbody>
        </table>
        </div>
</div>
<?php require RUTAAPP . '/vistas/includes/footer.php';?>