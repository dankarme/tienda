<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php';?>
 <script>
            function soloLetras(e) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
        <script>
            function soloNumeros(n) {
                key = n.keyCode || n.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " 1234567890,";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
<div class="container">
    <div class="row">
        <div class="col s12">
         <div class="card">
           <div class="card-content black-text">
               <span class="card-title black-text">Editar Perfil</span>
               <form action="<?php echo RUTAPUBLIC; ?>/perfiles/editarPerfil/<?php echo $data['cedula'] ?>" method="post">
                   <input type="text" name="nombres" placeholder="nombres" value="<?php echo $data['nombres']; ?>" onkeypress=" return soloLetras(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <input type="text" name="apellidos" placeholder="apellidos" value="<?php echo $data['apellidos']; ?>" onkeypress=" return soloLetras(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <input type="text" name="direccion" placeholder="direccion" value="<?php echo $data['direccion']; ?>" onkeypress=" return soloLetras(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <input type="email" name="correo" placeholder=correo" value="<?php echo $data['correo']; ?>" onkeypress=" return soloLetras(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <input type="text" name="telefono1" placeholder="telefono1" value="<?php echo $data['telefono1']; ?>" onkeypress=" return soloNumeros(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <input type="text" name="telefono2" placeholder="telefono2" value="<?php echo $data['telefono2']; ?>" onkeypress=" return soloNumeros(event)" 
                       oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                       required oninput="setCustomValidity('')"/>
                   <button class="btn waves-effect waves-light black" type="submit" name="action">
                       Actualizar<i class="material-icons">send</i></button>
               </form>
        </div> 
        </div>   
        </div>   
    </div>   
</div>
