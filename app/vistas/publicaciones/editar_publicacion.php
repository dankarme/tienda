<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php';?>
<script>
            function soloLetras(e) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
        <script>
            function soloNumeros(n) {
                key = n.keyCode || n.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " 1234567890,";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
<div class="container">
    <div class="row">
        <div class="col s12">
         <div class="card">
           <div class="card-content black-text">
               <span class="card-title black-text">Editar Publicacion</span>
               <form action="<?php echo RUTAPUBLIC; ?>/publicaciones/editarPublicacion/<?php echo $data['id_producto'] ?>" method="post" enctype="multipart/form-data">
                   
                   <input type="text" name="nombre_prod" placeholder="Nombre" value=" <?php echo $data['nombre_prod']; ?> "required onkeypress ="return soloLetras(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="color" placeholder="Color" value="<?php echo $data['color']; ?>"required onkeypress ="return soloLetras(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="talla" placeholder="Talla" value="<?php echo $data['talla']; ?>"required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="precio" placeholder="Precio" value="<?php echo $data['precio']; ?>"required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="stock" placeholder="Stock" value="<?php echo $data['stock']; ?>"required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <img src="<?php echo RUTAPUBLIC; ?>/public/imagenes/<?php echo $data['nombre_img'] ?>" width="70">
                   <input type="hidden" name="ruta" value="<?php echo $data['nombre_img']; ?>">
                   <div class="file-field input-field">
                       <div class="btn black">
                           <span>Imagen</span>
                           <input type="file" name="imagen">
                       </div>  
                      
                   </div>
                   <button class="btn waves-effect waves-light black" type="submit" name="action">
                       Actualizar<i class="material-icons">send</i></button>
               </form>
        </div> 
        </div>   
        </div>   
    </div>   
</div>
