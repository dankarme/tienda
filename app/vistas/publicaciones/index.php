<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php'; ?>
<script>
    function soloNumeros(n) {
        key = n.keyCode || n.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " 1234567890,";
        especiales = [8, 37, 39, 46];

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
</script>
<div class="card">
    <div class="card-content black-text" style="padding: 1px;">
        <span class="card-title black-text"><h2>Catálogo</h2></span>
        <?php
        if ($_SESSION['id_perfil'] == 1) {
            ?>
            <a href="<?php echo RUTAPUBLIC; ?>/publicaciones/vistaNuevaPublicacion"
               class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">add</i> </a>
               <?php
           }
           ?>
    </div>   
</div>

<div class="container">
    <div class="row">
        <?php foreach ($data['post'] as $post): ?>
            <?php if ($_SESSION['id_perfil'] != 1) { ?> 
                <div class="modal" id="exampleModal<?php echo $post->id_producto; ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <form method="POST" action="<?php echo RUTAPUBLIC; ?>/publicaciones/subirOrden">
                            <input type="hidden" name="id_perfil" value="<?php echo $_SESSION['id_usuario']; ?>">
                            <input type="hidden" name="id_producto" value="<?php echo$post->id_producto; ?>">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Orden</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="basic-url">Cantidad de orden</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="cantidad" placeholder="ingrese una cantidad" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               oninvalid="setCustomValidity('DEBE LLENAR ESTE CAMPO')"
                                               required oninput="setCustomValidity('')">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
        
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                    <img class="card-img-top" src="<?php echo RUTAPUBLIC; ?>/public/imagenes/<?php echo $post->foto_img; ?>" width="200">   
                
                <div class="card-body">
                    <span class="card-title activator grey-text text-darken-4">
                        <h5 class="card-title"><?php echo $post->nombre_prod; ?></h5>
                        <i class="material-icons right" data-toggle="modal" data-target="#exampleModal<?php echo $post->id_producto; ?>">more_vert</i>
                    </span>
                    <p class="card-text">PRECIO: $<?php echo $post->precio; ?></p>
                    <p class="card-text">TALLA: <?php echo $post->talla; ?></p>
                    <p class="card-text">COLOR: <?php echo $post->color; ?></p>
                    <p class="card-text">Cantidad: <?php echo $post->stock; ?></p>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"<?php echo $post->nombre_prod; ?>
                          <i class="material-icons right"></i></span>
                </div>
                <?php if ($_SESSION['id_perfil'] != 2) { ?>
                    <div class="card-action">
                        <a href="<?php echo RUTAPUBLIC; ?>/publicaciones/vistaEditarPublicacion/
                           <?php /* echo RUTAPUBLIC; */ echo $post->id_producto ?>">Editar</a>
                        <a href="<?php echo RUTAPUBLIC; ?>/publicaciones/eliminarPublicacion/ 
                           <?php echo $post->id_producto; ?>/<?php echo $post->foto_img ?>">Eliminar</a>
                    </div>
                <?php } ?>
            </div>
        </div>   
    <?php endforeach ?>
</div>
</div>
<?php require RUTAAPP . '/vistas/includes/footer.php'; ?>

