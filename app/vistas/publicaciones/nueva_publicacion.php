<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php require RUTAAPP . '/vistas/includes/header.php';?>
 <script>
            function soloLetras(e) {
                key = e.keyCode || e.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
        <script>
            function soloNumeros(n) {
                key = n.keyCode || n.which;
                tecla = String.fromCharCode(key).toLowerCase();
                letras = " 1234567890,";
                especiales = [8, 37, 39, 46];

                tecla_especial = false
                for (var i in especiales) {
                    if (key == especiales[i]) {
                        tecla_especial = true;
                        break;
                    }
                }
                if (letras.indexOf(tecla) == -1 && !tecla_especial)
                    return false;
            }
        </script>
<div class="container">
    <div class="row">
        <div class="col s12">
         <div class="card">
           <div class="card-content black-text">
               <span class="card-title black-text">Nuevos zapatos</span>
               <form action="<?php echo RUTAPUBLIC; ?>/publicaciones/nuevaPublicacion" method="post" enctype="multipart/form-data">
            <input type="text" name="nombre" placeholder="Nombre"required onkeypress ="return soloLetras(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
            <input type="text" name="color" placeholder="Color" required onkeypress ="return soloLetras(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="talla" placeholder="Talla"required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="precio" placeholder="Precio"required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <input type="text" name="stock" placeholder="Stock" required onkeypress ="return soloNumeros(event)"
                               oninvalid="setCustomValidity('El campo de nombres es obligatorio')"
                               oninput="setCustomValidity('')"/>
                   <div class="file-field input-field">
                       <div class="btn black">
                           <span>Imagen</span>
                           <input type="file" name="imagen">
                       </div>  
                   
                   </div>
                   <button class="btn waves-effect waves-light black" type="submit" name="action">
                       Publicar<i class="material-icons">send</i></button>
               </form>
        </div> 
        </div>   
        </div>   
    </div>   
</div>

<?php require RUTAAPP . '/vistas/includes/footer.php';?>
